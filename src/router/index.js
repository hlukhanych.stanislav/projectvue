import { createRouter, createWebHistory } from 'vue-router'
import HomePage from "@/components/HomePage.vue";
import SignPage from "@/components/SignPage.vue";
import ClientPage from "@/components/ClientPage.vue";
import CatalogPage from "@/components/CatalogPage.vue";
import FavoritesPage from "@/components/FavoritesPage.vue";
import LaptopShow from "@/components/LaptopShow.vue";
import CartPage from "@/components/CartPage.vue";
import AddPage from "@/components/AddPage.vue";

const routes = [
  {
    path: '/',
    component: HomePage,
    name: 'home'
  },
  {
    path: '/sign',
    component: SignPage,
    name: 'sign'
  },
  {
    path: '/client',
    component: ClientPage,
    name: 'client'
  },
  {
    path: '/catalog',
    component: CatalogPage,
    name: 'catalog'
  },
  {
    path: '/favorites',
    component: FavoritesPage,
    name: 'favorites'
  },
  {
    path: '/:id',
    name: 'laptop',
    component: LaptopShow
  },
  {
    path: '/cart',
    name: 'cart',
    component: CartPage
  },
  {
    path: '/add',
    name: 'add',
    component: AddPage
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
