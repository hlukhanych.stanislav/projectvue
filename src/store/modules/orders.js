import getModuleSettingsObject from "@/store/helpers/GetModuleSettingsObject";
export default {
    ...getModuleSettingsObject('orders')
}