import { createStore } from 'vuex'
import auth from './auth.js';
import laptops from "@/store/modules/laptops";
import orders from "@/store/modules/orders";
export default createStore({
  state: {
    products: [],
    favorites: [],
  },
  getters: {
    getProducts: (state) => state.products,
    getFavorites: (state) => state.favorites,
  },
  mutations: {
    addProduct(state, prod) {
      if (!state.products.some(item => item.id === prod.id)) {
        state.products.push(prod);
        alert('Додано в кошик!')
      }
      else {
        alert('Даний товар вже є в кошику')
      }
    },

    addFavorite(state, prod) {
      if (!state.favorites.some(item => item.id === prod.id)) {
        state.favorites.push(prod);
        alert('Додано в обрані!')
      }
      else {
        alert('Даний товар вже є в обраних')
      }
    },

    deleteProduct(state, prodId) {
      state.products = state.products.filter((prod) => prod.id !== prodId)
    },

    deleteFavorite(state, prodId) {
      state.favorites = state.favorites.filter((prod) => prod.id !== prodId)
    },

    clearProducts(state) {
      state.products = []
    },

    clearFavorites(state) {
      state.favorites = []
    },
  },
  actions: {
    onAddProduct({commit}, prodObj) {
      commit('addProduct', {
        ...prodObj
      })
    },

    onAddFavorite({commit}, prodObj) {
      commit('addFavorite', {
        ...prodObj
      })
    },

    onDeleteProduct({commit}, idToDelete) {
      commit('deleteProduct', idToDelete)
    },

    onDeleteFavorite({commit}, idToDelete) {
      commit('deleteFavorite', idToDelete)
    },

    onClearProducts({commit}) {
      commit('clearProducts')
    },

    onClearFavorites({commit}) {
      commit('clearFavorites')
    }
  },
  namespaced: true,
  modules: {
    auth,
    laptops,
    orders,
  }
})
